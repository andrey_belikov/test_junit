package com.lineate.traineeship;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class EntityTest {

    @Parameterized.Parameters(name = "{index} : [{0}] -> {1}")
    public  static List names() {
        List list = Arrays.asList( new Object[][] {
                {null, false},
                {"Entity", true},
                {"Entity Name", false},
                {"", false},
                {" ", false},
                {"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", false},//>32 chars
        });

        return list;
    }

    @Parameterized.Parameter(0)
    public String nameInput;

    @Parameterized.Parameter(1)
    public boolean resultOutput;

    @Test
    void nameTest() {
        ServiceFactory serviceFactory = new ServiceFactory();
        UserService userService = serviceFactory.createUserService();
        EntityService entityService = serviceFactory.createEntityService();
        Group group = userService.createGroup("Users", new HashSet<>());
        User user = userService.createUser("User", group);
        assertEquals(resultOutput, entityService.createEntity(user, nameInput, "Value"));
    }
}
