package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class EntityServiceTest {

    static  ServiceFactory serviceFactory;
    static UserService userService;
    static EntityService entityService;

    @BeforeAll
    void createFactory(){
        serviceFactory = new ServiceFactory();
    }
    @BeforeEach
    void createUserService(){
        userService = serviceFactory.createUserService();
        entityService = serviceFactory.createEntityService();
    }

    @Test
    void createUserTest() {
        Collection<Permission> permissions = new ArrayList<Permission>(Arrays.asList(Permission.write, Permission.read));
        Group group = userService.createGroup("Users", permissions);
        assertEquals(group.getUsers(), 0);
        User user = userService.createUser("Tom", group);
        assertEquals(group.getUsers(), 1);
        entityService.createEntity(user, "Entity", "Value");
        assertEquals("Value", entityService.getEntityValue(user, "Entity"));
    }

    @Test
    void createUserWithEmptyNameTest() {
        Collection<Permission> permissions = new ArrayList<Permission>(Arrays.asList(Permission.write, Permission.read));
        Group group = userService.createGroup("Users", permissions);
        User user = userService.createUser(null, group);
        assertEquals(group.getUsers(), 0);
    }

    @Test
    void createUserWithoutGroupTest() {
        Collection<Permission> permissions = new ArrayList<Permission>(Arrays.asList(Permission.write, Permission.read));
        Group group = null;
        User user = userService.createUser("Tom", group);
        assertEquals(user, null);
    }

    @Test
    void creatGroupWithoutNameTest() {
        Collection<Permission> permissions = new ArrayList<Permission>(Arrays.asList(Permission.write, Permission.read));
        Group group = null;
        assertEquals(userService.createGroup(null, permissions), null);
    }

    @Test
    void creatGroupWithoutPermissionsTest() {
        Group group = null;
        assertEquals(userService.createGroup("Group", null), null);
    }

    @Test
    void creatEntityTest() {
        Collection<Permission> permissions = new ArrayList<Permission>(Arrays.asList(Permission.write, Permission.read));
        Group group = userService.createGroup("Users", permissions);
        User user = userService.createUser("Tom", group);
        assertFalse(entityService.createEntity(user, null, "Value"));
        assertFalse(entityService.createEntity(user, "", "Value"));
        assertFalse(entityService.createEntity(user, "Entity New", "Value"));
        StringBuilder entityName = new StringBuilder();
        while(entityName.length() < 33){
            entityName.append("a");
        }
        assertFalse(entityService.createEntity(user, entityName.toString(), "Value"));
        assertTrue(entityService.createEntity(user, "Entity", "Value"));
    }

    @Test
    void valuesTest() {
        Collection<Permission> permissions = new ArrayList<Permission>(Arrays.asList(Permission.write, Permission.read));
        Group group = userService.createGroup("Users", permissions);
        User user = userService.createUser("Tom", group);
        entityService.createEntity(user, "Entity", "Value");
        User user2 = userService.createUser("User2", group);
        assertEquals("Value", entityService.getEntityValue(user2, "Entity"));
        assertTrue(entityService.updateEntity(user2, "Entity", "OtherValue"));
        assertEquals("OtherValue", entityService.getEntityValue(user, "Entity"));
    }

    @Test
    void permissionsTest() {
        Collection<Permission> permissionsRead = new ArrayList<Permission>(Arrays.asList(Permission.read));
        Collection<Permission> permissionsWrite = new ArrayList<Permission>(Arrays.asList(Permission.write));
        assertNull(userService.createGroup("User", null));
        Group group = userService.createGroup("Users", permissionsRead);
        User user = userService.createUser("Tom", group);
        User user2 = userService.createUser("User2", group);
        entityService.createEntity(user, "Entity", "Value");
        assertFalse(entityService.updateEntity(user2, "Entity", "OtherValue"));
        assertEquals("Value", entityService.getEntityValue(user2, "Entity"));
        assertTrue(entityService.updateEntity(user, "Entity", "OtherValue"));
        assertEquals("OtherValue", entityService.getEntityValue(user, "Entity"));
    }

    @Test
    void repositoryTest() {
        EntityRepository repository = Mockito.mock(EntityRepository.class);
        EntityService entityService = serviceFactory.createEntityService(repository);

        Collection<Permission> permissions = new ArrayList<Permission>(Arrays.asList(Permission.write, Permission.read));
        Group group = userService.createGroup("Users", permissions);
        User user = userService.createUser("Tom", group);
        Entity entity = new EntityImplements(user, "Entity");
        entity.setValue("Value");

        Mockito.when(repository.get("Entity")).thenReturn(entity);

        assertEquals("Value", entityService.getEntityValue(user, "Entity"));
        assertTrue(entityService.updateEntity(user, "Entity", "OtherValue"));
        assertEquals("OtherValue", entityService.getEntityValue(user, "Entity"));
    }
}
