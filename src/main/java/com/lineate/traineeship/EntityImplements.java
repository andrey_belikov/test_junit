package com.lineate.traineeship;

import java.util.Collection;

public class EntityImplements implements Entity {

    private User owner;
    private String name;
    private String value;
    private Collection<Group> groups;

    public EntityImplements(User user, String name) {
    }

    @Override
    public User getOwner() {
        return owner;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Collection<Group> getGroups() {
        return groups;
    }
}
