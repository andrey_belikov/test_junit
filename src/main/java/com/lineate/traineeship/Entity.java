package com.lineate.traineeship;

import java.util.Collection;

public interface Entity {
    User getOwner();

    String getName();

    String getValue();

    void setValue(String value);

    Collection<Group> getGroups();
}
